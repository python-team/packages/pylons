pylons (1.0.3-2) UNRELEASED; urgency=medium

  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.1.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Ondřej Nový <onovy@debian.org>  Sat, 20 Jul 2019 00:19:33 +0200

pylons (1.0.3-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URL (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * d/watch: Use https protocol
  * d/changelog: Remove trailing whitespaces
  * Convert git repository from git-dpm to gbp layout

  [ Piotr Ożarowski ]
  * New upstream release
  * Replace move_data_outside_site-packages.patch with
    0001-move-media_path-outside-dist-packages.patch due to change in
    pastescript 2.0.2-1
  * Standards-Version bumped to 4.3.0 (no changes needed)
  * Remove Oleksandr from Uploaders. Thanks for all your work! (closes: 820134)

 -- Piotr Ożarowski <piotr@debian.org>  Sat, 29 Dec 2018 22:24:44 +0100

pylons (1.0.2-1) unstable; urgency=medium

  * New upstream release
  * dont_use_removed_exception_attribute patch removed, applied upstream
  * debian/rules uses now dh sequencer + pybuild build system
  * debian/watch: use pypi.debian.net redirector
  * Standards-Version bumped to 3.9.6 (no changes needed)

 -- Piotr Ożarowski <piotr@debian.org>  Wed, 22 Jul 2015 22:11:25 +0200

pylons (1.0.1-3) unstable; urgency=medium

  * Replace the WebOb exception patch by a more complete one from
    Darren Yin.

 -- Andrew Shadura <andrewsh@debian.org>  Wed, 08 Oct 2014 14:49:01 +0200

pylons (1.0.1-2) unstable; urgency=medium

  [ Andrew Shadura ]
  * Add a patch from Uriy Zhuravev so that we don't use exception attribute
    which WebOb 1.4 has removed.

 -- Piotr Ożarowski <piotr@debian.org>  Mon, 08 Sep 2014 22:16:31 +0200

pylons (1.0.1-1) unstable; urgency=low

  [ Piotr Ożarowski ]
  * New upstream release
    - no longer tries to import webob.multidict (closes: 721864)
    - add python-markupsafe to Depends
    - bump python-routes, python-beaker, python-paste, python-pastescript,
      python-pastedeploy, python-formencode, python-simplejson,
      python-decorator, python-nose, python-mako, python-weberror,
      python-webtest, python-tempita and python-webob's minimum required
      versions
  * ipython_0.11_compatibility patch removed (merged upstream)
  * Remove Pylons.egg-info, tests/test_units/cache and
    tests/test_units/session directories in clean target
  * quilt's targets no longer used in debian/rules
  * Standards-Version bumped to 3.9.5 (no changes needed)

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

 -- Piotr Ożarowski <piotr@debian.org>  Sun, 01 Dec 2013 21:00:37 +0100

pylons (1.0-2) unstable; urgency=low

  * Add ipython_0.11_compatibility patch (thanks to Julian Taylor)
  * Add build-arch and build-indep targets to debian/rules
  * Switch from dh_pysupport to dh_python2
  * Source format changed to 3.0 (quilt)
  * Standards-Version bumped to 3.9.2 (no changes needed)

 -- Piotr Ożarowski <piotr@debian.org>  Tue, 02 Aug 2011 21:17:36 +0200

pylons (1.0-1) experimental; urgency=low

  * New upstream release
  * python-pudge removed from Suggests (package is no longer in Debian)

 -- Piotr Ożarowski <piotr@debian.org>  Fri, 28 May 2010 20:51:33 +0200

pylons (1.0~rc1-1) experimental; urgency=low

  [ Oleksandr Moskalenko ]
  * debian/control: Switched maintainer to DPMT.
  * debian/copyright: Switched debian packaging license to BSD to sync with
    upstream.

  [ Piotr Ożarowski ]
  * New upstream release
    - python-routes minimum version bumped to 1.12
  * Install UPGRADING file in /usr/share/doc/python-pylons/

 -- Piotr Ożarowski <piotr@debian.org>  Tue, 02 Mar 2010 22:33:25 +0100

pylons (1.0~b1-1) experimental; urgency=low

  * New upstream release
  * python-beaker minimum version bumped to 1.3
  * python-jinja, *elementtree, python-turbokid and python-cherrypy3
    removed from Recommends/Suggests

 -- Piotr Ożarowski <piotr@debian.org>  Sat, 06 Feb 2010 17:48:36 +0100

pylons (0.10~b1-1) unstable; urgency=low

  * New upstream release
  * Bump Standards-Version to 3.8.4 (no changes needed)

 -- Piotr Ożarowski <piotr@debian.org>  Sat, 06 Feb 2010 17:20:58 +0100

pylons (0.9.7-2) unstable; urgency=low

  * Move templates to /usr/share/paster_templates (closes: #526767)
    - depend on python-pastescript >= 1.7.3-5
  * Move media files to /usr/share/python-pylons/
  * Bump Standards-Version to 3.8.3 (no changes needed)

 -- Piotr Ożarowski <piotr@debian.org>  Mon, 30 Nov 2009 00:26:47 +0100

pylons (0.9.7-1) unstable; urgency=low

  [ Oleksandr Moskalenko ]
  * New upstream release

  [ Piotr Ożarowski ]
  * Bump python-routes, python-webhelpers, python-beaker, python-pastedeploy,
    python-simplejson and python-webob minimum required versions
  * Set the minimum required SQLAlchemy version to >= 0.5
  * Add python-pkg-resources to Depends
  * debian/copyright updated (added a note about templates)

 -- Oleksandr Moskalenko <malex@debian.org>  Mon, 23 Feb 2009 21:10:38 +0100

pylons (0.9.7~rc6-1) experimental; urgency=low

  * New upstream release

 -- Piotr Ożarowski <piotr@debian.org>  Fri, 13 Feb 2009 20:08:50 +0100

pylons (0.9.7~rc5-1) experimental; urgency=low

  * New upstream release.
  * debian/control:
    - Updated depends versions for beaker, pastescript, formencode,
      simplejson, mako, webob, weberror, webtest.
    - Added suggests - python-jinja2, libapache2-mod-wsgi, python-cherrypy3,
      python-turbokid, python-migrate.
  * debian/copyright: Updated copyright information and link to GPL.

 -- Oleksandr Moskalenko <malex@debian.org>  Thu, 12 Feb 2009 11:21:06 -0700

pylons (0.9.7~rc4-1) experimental; urgency=low

  * New upstream release
  * Add python-webtest to Depends
  * Bump python-beaker, python-formencode, python-mako, python-pastescript and
    python-webhelpers minimum required versions
  * Add python-docutils to suggested packages

 -- Piotr Ożarowski <piotr@debian.org>  Tue, 25 Nov 2008 10:09:35 +0100

pylons (0.9.7~rc3-1) experimental; urgency=low

  [ Sandro Tosi ]
  * Switch Vcs-Browser field to viewsvn

  [ Piotr Ożarowski ]
  * New upstream release
  * bump python-beaker, python-paste, python-pastescript, python-formencode,
    python-simplejson, python-nose, python-webob and python-weberror minimum
    required versions

 -- Piotr Ożarowski <piotr@debian.org>  Thu, 06 Nov 2008 17:50:04 +0100

pylons (0.9.7~rc2-1) experimental; urgency=low

  * New upstream release
  * bump python-routes, python-beaker, python-webob and python-simplejson
    minimum required versions

 -- Piotr Ożarowski <piotr@debian.org>  Tue, 30 Sep 2008 20:05:28 +0200

pylons (0.9.7~rc1-1) experimental; urgency=low

  [ Christoph Haas ]
  * add python-tempita, python-webob and python-weberror to Depends
  * bump python-webhelpers, python-beaker, python-paste, python-pastescript,
    python-formencode, python-nose and python-mako minimum required versions

  [ Piotr Ożarowski ]
  * New upstream release
  * add python-jinja and ipython to Suggests
  * bump python-pastedeploy, python-decorator, python-mako, python-genshi and
    python-simplejson minimum required versions
  * tests are no longer installed
  * s/python-elementree/python-elementtree (without any typos this time,
    really closes: #485407)
  * Update debian/watch file to handle "rc" versions correctly

 -- Piotr Ożarowski <piotr@debian.org>  Fri, 15 Aug 2008 15:07:12 +0200

pylons (0.9.6.2-2) unstable; urgency=low

  * Typos fixed in:
    + debian/NEWS
    + debian/control (python-elementree in Suggests, closes: #485407)
  * python-nose required version bumped to 0.9.3 (LP: #182384)
  * Bump Standards-Version to 3.8.0 (no changes needed)

 -- Piotr Ożarowski <piotr@debian.org>  Tue, 08 Jul 2008 20:06:43 +0200

pylons (0.9.6.2-1) unstable; urgency=high

  [ Piotr Ożarowski ]
  * New upstream release (security fix)
    + minimum required python-routes' version bumped to 1.8,
      python-beaker's bumped to 0.8.1
  * Move python-myghty from Depends to Suggests
  * Move python-support to Build-Depends-Indep
  * Remove dpatch stuff, not needed anymore (thanks Christoph)
  * Standards-version bumped to 3.7.3

  [ Sandro Tosi ]
  * debian/control
    - uniforming Vcs-Browser field

 -- Piotr Ożarowski <piotr@debian.org>  Wed, 28 May 2008 22:42:46 +0200

pylons (0.9.6.1-1) unstable; urgency=low

  * New upstream release
  * Add Homepage field
  * Rename XS-Vcs-* fields to Vcs-* (dpkg supports them now)
  * Remove README.Debian as python-decorator is already in the archive
  * Remove debian/dirs file (no longer needed)
  * Bump required versions of: python-routes, python-beaker, python-paste,
    python-pastedeploy, python-pastescript, python-formencode,
    python-decorator and python-mako
  * Add python-{c,}elementtree, python-cheetah, python-kid, python-genshi,
    python-pygments and python-pudge to Suggests
  * Add myself to Uploaders

 -- Piotr Ożarowski <piotr@debian.org>  Thu, 04 Oct 2007 23:23:28 +0200

pylons (0.9.6-2) unstable; urgency=low

  * debian/control: Updated dependency on python-webhelpers (Closes: #443835).

 -- Oleksandr Moskalenko <malex@debian.org>  Wed, 26 Sep 2007 10:49:28 -0600

pylons (0.9.6-1) unstable; urgency=low

  * New upstream release.

 -- Oleksandr Moskalenko <malex@debian.org>  Mon, 10 Sep 2007 14:23:38 -0600

pylons (0.9.5-7) unstable; urgency=low

  * Move tests to documentation (Closes: #433374).

 -- Oleksandr Moskalenko <malex@debian.org>  Mon, 16 Jul 2007 14:47:06 -0600

pylons (0.9.5-6) unstable; urgency=low

  * debian/control: Added a dependency on dpatch.
  * debian/rules: Added rules to handle source patching at build-time using
    dpatch.
  * debian/patches/01-validate_unicode.dpatch: To fix a unicode validation
    problem added a patch provided by "Dwayne C. Litzenberger"
    <dlitz@dlitz.net> (Closes: #427672).

 -- Oleksandr Moskalenko <malex@debian.org>  Tue, 05 Jun 2007 13:07:05 -0600

pylons (0.9.5-5) unstable; urgency=low

  * debian/control: Added python-mako to depends (Closes: #425332).

 -- Oleksandr Moskalenko <malex@debian.org>  Mon, 21 May 2007 13:10:30 -0600

pylons (0.9.5-4) unstable; urgency=low

  * Updated rules and control for new python-support with automated .egg.info
    rename (Closes: #423793).

 -- Oleksandr Moskalenko <malex@debian.org>  Mon, 14 May 2007 09:26:54 -0600

pylons (0.9.5-3) unstable; urgency=low

  * Upload into unstable now that python-decorator is through the NEW queue.
  * debian/control: Added a dependency on python-decorator.

 -- Oleksandr Moskalenko <malex@debian.org>  Mon, 07 May 2007 15:38:33 -0600

pylons (0.9.5-2) experimental; urgency=low

  * debian/control: Temporarily removed the dependency on python-decorator. I
    suppose having pylons uninstallable won't help people to see the README
    about the decorator package.

 -- Oleksandr Moskalenko <malex@debian.org>  Fri, 13 Apr 2007 19:31:07 -0600

pylons (0.9.5-1) experimental; urgency=low

  * New upstream release.
  * README.Debian: Added a readme about where to obtain python-decorator
    package needed by this pylons release.
  * debian/control: Added a dependency on python-decorator. Pylons is not
    going to be installable until decorator gets through the NEW queue, so I"m
    uploading it to the experimental and making python-decorator available
    separately. See README.Debian for complete information.

 -- Oleksandr Moskalenko <malex@debian.org>  Fri, 13 Apr 2007 12:28:12 -0600

pylons (0.9.4.1-4) unstable; urgency=low

  * Etch is released. Upload into unstable.

 -- Oleksandr Moskalenko <malex@debian.org>  Tue, 10 Apr 2007 12:32:29 -0600

pylons (0.9.4.1-3) experimental; urgency=low

  * Updated package dependencies (Closes: #413514).

 -- Oleksandr Moskalenko <malex@debian.org>  Mon,  5 Mar 2007 10:13:47 -0700

pylons (0.9.4.1-2) experimental; urgency=low

  * debian/control:
    - Added XS-Vcs-Svn and XS-Vcs-Browser control fields.

 -- Oleksandr Moskalenko <malex@debian.org>  Mon, 29 Jan 2007 18:29:13 -0700

pylons (0.9.4.1-1) experimental; urgency=low

  * New upstream version bugfix release.

 -- Oleksandr Moskalenko <malex@debian.org>  Mon, 22 Jan 2007 10:32:11 -0700

pylons (0.9.4-1) experimental; urgency=low

  * New upstream version (Closes: #406649).
  * debian/control:
    - Updated version dependencies that changed with 0.9.4.
    - I joined the Debian Python Modules Team, so added
      <python-modules-team@lists.alioth.debian.org> to Uploaders.

 -- Oleksandr Moskalenko <malex@debian.org>  Fri, 12 Jan 2007 10:23:16 -0700

pylons (0.9.3-2) unstable; urgency=low

  * Fix a typo in package description (Closes: #402518).

 -- Oleksandr Moskalenko <malex@debian.org>  Thu, 14 Dec 2006 08:12:28 -0700

pylons (0.9.3-1) unstable; urgency=low

  * Initial release (Closes: #392482).

 -- Oleksandr Moskalenko <malex@debian.org>  Sun, 12 Nov 2006 21:44:37 -0700
